# translation of ksmserver.po to Latvian
# Copyright (C) 2007, 2008 Free Software Foundation, Inc.
#
# Maris Nartiss <maris.kde@gmail.com>, 2007, 2008.
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008.
# Viesturs Zariņš <viesturs.zarins@mii.lu.lv>, 2009.
# Rūdofls Mazurs <rudolfs.mazurs@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-15 02:12+0000\n"
"PO-Revision-Date: 2011-07-07 23:06+0300\n"
"Last-Translator: Rūdofls Mazurs <rudolfs.mazurs@gmail.com>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "Atslēgšanos atcēla '%1'"

#: main.cpp:77
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:81 main.cpp:89
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:84
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:91
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:95
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:98
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:111 main.cpp:146
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:113 main.cpp:148
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:121 main.cpp:134
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:124 main.cpp:137
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:152
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:155
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:162
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:196
#, fuzzy, kde-format
#| msgid ""
#| "The reliable KDE session manager that talks the standard X11R6 \n"
#| "session management protocol (XSMP)."
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"Uzticamais KDE sesiju pārvaldnieks, kas darbojas ar standarta X11R6\n"
"sesiju vadības protokolu (XSMP)."

#: main.cpp:200
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Ja iespējams, atjauno iepriekšējo sesiju"

#: main.cpp:203
#, kde-format
msgid "Also allow remote connections"
msgstr "Atļaut arī attālinātus savienojumus"

#: main.cpp:206
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:210
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:884
#, fuzzy, kde-format
#| msgid "The KDE Session Manager"
msgid "Session Management"
msgstr "KDE sesiju pārvaldnieks"

#: server.cpp:889
#, kde-format
msgid "Log Out"
msgstr "Atteikties"

#: server.cpp:894
#, kde-format
msgid "Shut Down"
msgstr ""

#: server.cpp:899
#, kde-format
msgid "Reboot"
msgstr ""

#: server.cpp:905
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "Atteikties bez apstiprinājuma"

#: server.cpp:910
#, fuzzy, kde-format
#| msgid "Halt Without Confirmation"
msgid "Shut Down Without Confirmation"
msgstr "Izslēgt bez apstiprinājuma"

#: server.cpp:915
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "Pārstartēt bez apstiprinājuma"

#, fuzzy
#~| msgid ""
#~| "Starts 'wm' in case no other window manager is \n"
#~| "participating in the session. Default is 'kwin'"
#~ msgid ""
#~ "Starts <wm> in case no other window manager is \n"
#~ "participating in the session. Default is 'kwin'"
#~ msgstr ""
#~ "Palaiž `wm` gadījumā, ja neviens cits logu pārvaldnieks\n"
#~ "nepiedalās sesijā. Noklusētais ir `kwin`"

#, fuzzy
#~| msgid "&Logout"
#~ msgid "Logout"
#~ msgstr "&Atteikties"

#, fuzzy
#~| msgid "Logging out in 1 second."
#~| msgid_plural "Logging out in %1 seconds."
#~ msgid "Sleeping in 1 second"
#~ msgid_plural "Sleeping in %1 seconds"
#~ msgstr[0] "Atteiksies pēc %1 sekundes."
#~ msgstr[1] "Atteiksies pēc %1 sekundēm."
#~ msgstr[2] "Atteiksies pēc %1 sekundēm."

#~ msgid "Logging out in 1 second."
#~ msgid_plural "Logging out in %1 seconds."
#~ msgstr[0] "Atteiksies pēc %1 sekundes."
#~ msgstr[1] "Atteiksies pēc %1 sekundēm."
#~ msgstr[2] "Atteiksies pēc %1 sekundēm."

#~ msgid "Turning off computer in 1 second."
#~ msgid_plural "Turning off computer in %1 seconds."
#~ msgstr[0] "Izslēgs datoru pēc %1 sekundes."
#~ msgstr[1] "Izslēgs datoru pēc %1 sekundēm."
#~ msgstr[2] "Izslēgs datoru pēc %1 sekundēm."

#~ msgid "Restarting computer in 1 second."
#~ msgid_plural "Restarting computer in %1 seconds."
#~ msgstr[0] "Pārstartēs datoru pēc %1 sekundes."
#~ msgstr[1] "Pārstartēs datoru pēc %1 sekundēm."
#~ msgstr[2] "Pārstartēs datoru pēc %1 sekundēm."

#, fuzzy
#~| msgid "&Turn Off Computer"
#~ msgid "Turn Off Computer"
#~ msgstr "&Izslēgt datoru"

#, fuzzy
#~| msgid "&Restart Computer"
#~ msgid "Restart Computer"
#~ msgstr "Pā&rstartēt datoru"

#~ msgctxt "default option in boot loader"
#~ msgid " (default)"
#~ msgstr " (noklusētais)"

#, fuzzy
#~| msgid "&Cancel"
#~ msgid "Cancel"
#~ msgstr "At&celt"

#~ msgid "&Standby"
#~ msgstr "Ie&midzināt"

#~ msgid "Suspend to &RAM"
#~ msgstr "Sastindzināt uz &RAM"

#~ msgid "Suspend to &Disk"
#~ msgstr "Sastindzināt uz &disku"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Andris Maziks, Aleksejs Zosims"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "andris.m@delfi.lv, locale@aleksejs.id.lv"

#~ msgid "(C) 2000, The KDE Developers"
#~ msgstr "(C) 2000, The KDE izstrādātāji"

#~ msgid "Matthias Ettrich"
#~ msgstr "Matiass Ettihs (Matthias Ettrich)"

#~ msgid "Luboš Luňák"
#~ msgstr "Lubošs Lunaks (Luboš Luňák)"

#~ msgid "Maintainer"
#~ msgstr "Uzturētājs"

#~ msgctxt "current option in boot loader"
#~ msgid " (current)"
#~ msgstr " (pašreizējais)"
