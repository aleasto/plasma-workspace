#  <tr@erdfunkstelle.de>, 2008.
# Thomas Reitelbach <tr@erdfunkstelle.de>, 2008, 2009.
# Burkhard Lück <lueck@hube-lueck.de>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_locations\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-01 00:49+0000\n"
"PO-Revision-Date: 2013-01-22 22:01+0100\n"
"Last-Translator: Burkhard Lück <lueck@hube-lueck.de>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: locationrunner.cpp:33
#, kde-format
msgid ""
"Finds local directories and files, network locations and Internet sites with "
"paths matching :q:."
msgstr ""
"Findet lokale Ordner und Dateien, Netzwerk-Orte und Internetseiten mit "
"Pfaden, die auf :q: passen."

#: locationrunner.cpp:59
#, kde-format
msgid "Open %1"
msgstr "„%1“ öffnen"

#: locationrunner.cpp:78 locationrunner.cpp:82
#, kde-format
msgid "Launch with %1"
msgstr "Starten mit %1"

#: locationrunner.cpp:88
#, kde-format
msgid "Go to %1"
msgstr "Gehe zu „%1“"

#: locationrunner.cpp:96
#, kde-format
msgid "Send email to %1"
msgstr "E-Mail senden an %1"

#~ msgid "Locations"
#~ msgstr "Orte"
