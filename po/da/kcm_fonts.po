# translation of kcmfonts.po to
# Danish translation of kcmfonts
# Copyright (C).
#
# Erik Kjær Pedersen <erik@binghamton.edu>, 2000, 2002, 2003, 2004, 2006.
# Martin Schlander <mschlander@opensuse.org>, 2008, 2010, 2011, 2012, 2015, 2017, 2018, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kcmfonts\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:33+0000\n"
"PO-Revision-Date: 2020-10-04 17:19+0200\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: label, entry (forceFontDPIWayland), group (General)
#: fontsaasettingsbase.kcfg:9
#, kde-format
msgid "Force font DPI Wayland"
msgstr "Gennemtving skrifttypens DPI Wayland"

#. i18n: ectx: label, entry (forceFontDPI), group (General)
#: fontsaasettingsbase.kcfg:13
#, fuzzy, kde-format
#| msgid "Force font DPI X11"
msgid "Force font DPI on X11"
msgstr "Gennemtving skrifttypens DPI X11"

#. i18n: ectx: label, entry (font), group (General)
#: fontssettings.kcfg:9
#, kde-format
msgid "General font"
msgstr "Generel skrifttype"

#. i18n: ectx: label, entry (fixed), group (General)
#: fontssettings.kcfg:21
#, kde-format
msgid "Fixed width font"
msgstr "Skrifttype med fast bredde"

#. i18n: ectx: label, entry (smallestReadableFont), group (General)
#: fontssettings.kcfg:33
#, kde-format
msgid "Small font"
msgstr "Lille skrifttype"

#. i18n: ectx: label, entry (toolBarFont), group (General)
#: fontssettings.kcfg:45
#, kde-format
msgid "Tool bar font"
msgstr "Skrifttype til værktøjslinje"

#. i18n: ectx: label, entry (menuFont), group (General)
#: fontssettings.kcfg:57
#, kde-format
msgid "Menu font"
msgstr "Skrifttype til menu"

#. i18n: ectx: label, entry (activeFont), group (WM)
#: fontssettings.kcfg:71
#, kde-format
msgid "Window title font"
msgstr "Skrifttype til vinduestitel"

#: kxftconfig.cpp:458
#, kde-format
msgctxt "use system subpixel setting"
msgid "Vendor default"
msgstr "Leverandørstandard"

#: kxftconfig.cpp:460
#, kde-format
msgctxt "no subpixel rendering"
msgid "None"
msgstr "Ingen"

#: kxftconfig.cpp:462
#, kde-format
msgid "RGB"
msgstr "RGB"

#: kxftconfig.cpp:464
#, kde-format
msgid "BGR"
msgstr "BGR"

#: kxftconfig.cpp:466
#, kde-format
msgid "Vertical RGB"
msgstr "Lodret RGB"

#: kxftconfig.cpp:468
#, kde-format
msgid "Vertical BGR"
msgstr "Lodret BGR"

#: kxftconfig.cpp:496
#, kde-format
msgctxt "use system hinting settings"
msgid "Vendor default"
msgstr "Leverandørstandard"

#: kxftconfig.cpp:498
#, kde-format
msgctxt "medium hinting"
msgid "Medium"
msgstr "Mellem"

#: kxftconfig.cpp:500
#, kde-format
msgctxt "no hinting"
msgid "None"
msgstr "Ingen"

#: kxftconfig.cpp:502
#, kde-format
msgctxt "slight hinting"
msgid "Slight"
msgstr "Lidt"

#: kxftconfig.cpp:504
#, kde-format
msgctxt "full hinting"
msgid "Full"
msgstr "Fuld"

#: package/contents/ui/main.qml:24
#, fuzzy, kde-format
#| msgid "&Adjust All Fonts..."
msgid "Adjust Global Scale…"
msgstr "&Justér alle skrifttyper..."

#: package/contents/ui/main.qml:35
#, kde-format
msgid ""
"Some changes such as anti-aliasing or DPI will only affect newly started "
"applications."
msgstr ""
"Visse ændringer såsom anti-alias eller DPI kommer kun til at påvirke "
"nystartede program."

#: package/contents/ui/main.qml:49
#, fuzzy, kde-format
#| msgid ""
#| "Very large fonts may produce odd-looking results. Consider adjusting the "
#| "global screen scale instead of using a very large font size."
msgid ""
"Very large fonts may produce odd-looking results. Instead of using a very "
"large font size, consider adjusting the global screen scale."
msgstr ""
"Meget store skrifttyper kan give resultater der ser mærkelige ud. Overvej at "
"justere den globale skærmskalering i stedet for at bruge en meget stor "
"skriftstørrelse."

#: package/contents/ui/main.qml:70
#, kde-format
msgid ""
"Decimal font sizes can cause text layout problems in some applications. "
"Consider using only integer font sizes."
msgstr ""

#: package/contents/ui/main.qml:92
#, kde-format
msgid ""
"The recommended way to scale the user interface is using the global screen "
"scaling feature."
msgstr ""
"Den anbefalede måde at skalere brugerfladen på er at bruge funktionen til "
"global skalering af skærmen."

#: package/contents/ui/main.qml:104
#, fuzzy, kde-format
#| msgid "&Adjust All Fonts..."
msgid "&Adjust All Fonts…"
msgstr "&Justér alle skrifttyper..."

#: package/contents/ui/main.qml:117
#, kde-format
msgid "General:"
msgstr "Generelt:"

#: package/contents/ui/main.qml:118
#, fuzzy, kde-format
#| msgid "General font"
msgid "Select general font"
msgstr "Generel skrifttype"

#: package/contents/ui/main.qml:129
#, kde-format
msgid "Fixed width:"
msgstr "Fast bredde:"

#: package/contents/ui/main.qml:130
#, fuzzy, kde-format
#| msgid "Fixed width font"
msgid "Select fixed width font"
msgstr "Skrifttype med fast bredde"

#: package/contents/ui/main.qml:141
#, kde-format
msgid "Small:"
msgstr "Lille:"

#: package/contents/ui/main.qml:142
#, fuzzy, kde-format
#| msgid "Small font"
msgid "Select small font"
msgstr "Lille skrifttype"

#: package/contents/ui/main.qml:153
#, kde-format
msgid "Toolbar:"
msgstr "Værktøjslinje:"

#: package/contents/ui/main.qml:154
#, fuzzy, kde-format
#| msgid "Tool bar font"
msgid "Select toolbar font"
msgstr "Skrifttype til værktøjslinje"

#: package/contents/ui/main.qml:165
#, kde-format
msgid "Menu:"
msgstr "Menu:"

#: package/contents/ui/main.qml:166
#, fuzzy, kde-format
#| msgid "Select Font"
msgid "Select menu font"
msgstr "Vælg skrifttype"

#: package/contents/ui/main.qml:176
#, kde-format
msgid "Window title:"
msgstr "Vinduestitel:"

#: package/contents/ui/main.qml:177
#, fuzzy, kde-format
#| msgid "Window title font"
msgid "Select window title font"
msgstr "Skrifttype til vinduestitel"

#: package/contents/ui/main.qml:192
#, kde-format
msgid "Anti-Aliasing:"
msgstr "Anti-aliasing:"

#: package/contents/ui/main.qml:197
#, kde-format
msgid "Enable"
msgstr "Aktivér"

#: package/contents/ui/main.qml:201
#, kde-kuit-format
msgctxt "@info:tooltip Anti-Aliasing"
msgid ""
"Pixels on displays are generally aligned in a grid. Therefore shapes of "
"fonts that do not align with this grid will look blocky and wrong unless "
"<emphasis>anti-aliasing</emphasis> techniques are used to reduce this "
"effect. You generally want to keep this option enabled unless it causes "
"problems."
msgstr ""

#: package/contents/ui/main.qml:215
#, kde-format
msgid "Exclude range from anti-aliasing"
msgstr "Ekskludér område fra anti-aliasing"

#: package/contents/ui/main.qml:234 package/contents/ui/main.qml:257
#, kde-format
msgid "%1 pt"
msgstr "%1 pt"

#: package/contents/ui/main.qml:249
#, kde-format
msgid "to"
msgstr "til"

#: package/contents/ui/main.qml:280
#, kde-format
msgctxt "Used as a noun, and precedes a combobox full of options"
msgid "Sub-pixel rendering:"
msgstr "Sub-pixel-rendering:"

#: package/contents/ui/main.qml:322
#, kde-kuit-format
msgctxt "@info:tooltip Sub-pixel rendering"
msgid ""
"<para>On TFT or LCD screens every single pixel is actually composed of three "
"or four smaller monochrome lights. These <emphasis>sub-pixels</emphasis> can "
"be changed independently to further improve the quality of displayed fonts.</"
"para> <para>The rendering quality is only improved if the selection matches "
"the manner in which the sub-pixels of your display are aligned. Most "
"displays have a linear ordering of <emphasis>RGB</emphasis> sub-pixels, some "
"have <emphasis>BGR</emphasis> and some exotic orderings are not supported by "
"this feature.</para>This does not work with CRT monitors."
msgstr ""

#: package/contents/ui/main.qml:327
#, kde-format
msgctxt "Used as a noun, and precedes a combobox full of options"
msgid "Hinting:"
msgstr "Hinting:"

#: package/contents/ui/main.qml:368
#, kde-kuit-format
msgctxt "@info:tooltip Hinting"
msgid ""
"Hinting is a technique in which hints embedded in a font are used to enhance "
"the rendering quality especially at small sizes. Stronger hinting generally "
"leads to sharper edges but the small letters will less closely resemble "
"their shape at big sizes."
msgstr ""

#: package/contents/ui/main.qml:378
#, kde-format
msgid "Force font DPI:"
msgstr "Gennemtving skrifttypens DPI:"

#: package/contents/ui/main.qml:420
#, kde-kuit-format
msgctxt "@info:tooltip Force fonts DPI"
msgid ""
"<para>Enter your screen's DPI here to make on-screen fonts match their "
"physical sizes when printed. Changing this option from its default value "
"will conflict with many apps; some icons and images may not scale as "
"expected.</para><para>To increase text size, change the size of the fonts "
"above. To scale everything, use the scaling slider on the <interface>Display "
"& Monitor</interface> page.</para>"
msgstr ""

#: package/contents/ui/main.qml:426
#, kde-format
msgid "Select Font"
msgstr "Vælg skrifttype"

#~ msgid "This module lets you configure the system fonts."
#~ msgstr "Dette modul lader dig konfigurere systemskrifttyperne."

#, fuzzy
#~| msgid "Change Display Scaling..."
#~ msgid "Change Display Scaling…"
#~ msgstr "Skift skærmskalering..."

#, fuzzy
#~| msgid ""
#~| "<p>This option forces a specific DPI value for fonts. It may be useful "
#~| "when the real DPI of the hardware is not detected properly and it is "
#~| "also often misused when poor quality fonts are used that do not look "
#~| "well with DPI values other than 96 or 120 DPI.</p><p>The use of this "
#~| "option is generally discouraged. For selecting proper DPI value a better "
#~| "option is explicitly configuring it for the whole X server if possible "
#~| "(e.g. DisplaySize in xorg.conf). When fonts do not render properly with "
#~| "real DPI value better fonts should be used or configuration of font "
#~| "hinting should be checked.</p>"
#~ msgctxt "@info:tooltip Force fonts DPI"
#~ msgid ""
#~ "<para>This option forces a specific DPI value for fonts. It may be useful "
#~ "when the real DPI of the hardware is not detected properly and it is also "
#~ "often misused when poor quality fonts are used that do not look well with "
#~ "DPI values other than 96 or 120 DPI.</para><para>The use of this option "
#~ "is generally discouraged.</para><para>If you are using the <emphasis>X "
#~ "Window System</emphasis>, for selecting the proper DPI value a better "
#~ "option is explicitly configuring it for the whole X server if possible (e."
#~ "g. DisplaySize in xorg.conf). When fonts do not render properly with the "
#~ "real DPI value better fonts should be used or configuration of font "
#~ "hinting should be checked.</para>"
#~ msgstr ""
#~ "<p>Dette tilvalg påtvinger at et vist antal punkter per tomme bruges for "
#~ "en skrifttype. Dette kan være nyttigt når hardwarens rigtige antal "
#~ "punkter per tomme ikke detekteres rigtigt, og bruges også ofte fejlagtigt "
#~ "når skrifttyper af dårlig kvalitet bruges, som ikke ser godt ud med andre "
#~ "værdier end 96 eller 120 punkter per tomme.</p><p>Brug af dette tilvalg "
#~ "frarådes i almindelighed. Et bedre alternativ for at vælge en rigtig "
#~ "værdi på antal punkter per tomme er eksplicit at indstille det for hele X-"
#~ "serveren om muligt (f.eks. DisplaySize i xorg.conf). Når skrifttyper ikke "
#~ "vises rigtigt med rigtige værdier for punkter per tomme, bør bedre "
#~ "skrifttyper bruges, ellers bør indstillingen af skrifttypesantydning "
#~ "kontrolleres.</p>"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Martin Schlander"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mschlander@opensuse.org"

#~ msgid "Fonts"
#~ msgstr "Skrifttyper"

#~ msgid "Antonis Tsiapaliokas"
#~ msgstr "Antonis Tsiapaliokas"

#~ msgctxt "@action:button"
#~ msgid "Show Contextual Help"
#~ msgstr "Vis kontekstuel hjælp"

#~ msgid "Select %1 Font..."
#~ msgstr "Vælg %1 skrifttype..."

#~ msgid "Font Settings Changed"
#~ msgstr "Skrifttype-indstillingerne blev ændret"

#~ msgid ""
#~ "<p>Some changes such as DPI will only affect newly started applications.</"
#~ "p>"
#~ msgstr ""
#~ "<p>Visse ændringer såsom DPI vil kun påvirke nystartede programmer.</p>"

#, fuzzy
#~| msgid "Vendor Default"
#~ msgid "Vendor default"
#~ msgstr "Leverandørstandard"

#~ msgid "Disabled"
#~ msgstr "Deaktiveret"

#, fuzzy
#~| msgid "Configure Fonts"
#~ msgid "Configure the system fonts"
#~ msgstr "Indstil skrifttyper"

#~ msgid "Choose..."
#~ msgstr "Vælg..."

#~ msgid "Configure Anti-Alias Settings"
#~ msgstr "Indstil anti-alias"

#~ msgid "E&xclude range:"
#~ msgstr "E&kskludér område:"

#~ msgctxt "abbreviation for unit of points"
#~ msgid " pt"
#~ msgstr " pt"

#~ msgid ""
#~ "<p>If you have a TFT or LCD screen you can further improve the quality of "
#~ "displayed fonts by selecting this option.<br />Sub-pixel rendering is "
#~ "also known as ClearType(tm).<br /> In order for sub-pixel rendering to "
#~ "work correctly you need to know how the sub-pixels of your display are "
#~ "aligned.</p> <p>On TFT or LCD displays a single pixel is actually "
#~ "composed of three sub-pixels, red, green and blue. Most displays have a "
#~ "linear ordering of RGB sub-pixel, some have BGR.<br /> This feature does "
#~ "not work with CRT monitors.</p>"
#~ msgstr ""
#~ "<p>Hvis du har en TFT- eller LCD-skærm kan du forøge kvaliteten af viste "
#~ "skrifttyper ved at vælge denne indstilling.<br />Sub-pixel-rendering er "
#~ "også kendt som ClearType(tm).<br /> For at sub-pixel-rendering kan virke "
#~ "korrekt skal du vide hvordan sub-pixels for din skærm er placeret ifht. "
#~ "hinanden. </p><p>På TFT- eller LCD-skærme er en enkelt pixel faktisk "
#~ "udgjort af tre sub-pixels, rød, grøn og blå. De fleste skærme har en "
#~ "lineær ordning af RGB-sub-pixels, nogle har BGR.<br /> Denne funktion "
#~ "virker ikke med CRT-skærme. </p>"

#~ msgid ""
#~ "Hinting is a process used to enhance the quality of fonts at small sizes."
#~ msgstr ""
#~ "Hint er en proces der bruges til at udbedre kvaliteten af skrifttyper i "
#~ "små størrelser."

#~ msgid "Used for normal text (e.g. button labels, list items)."
#~ msgstr "Brugt til normal tekst (f.eks. knap-etiketter og listepunkter)."

#~ msgid "A non-proportional font (i.e. typewriter font)."
#~ msgstr "En ikke-proportional skrifttype (f.eks. skrivemaskine-skrift)."

#~ msgid "Smallest font that is still readable well."
#~ msgstr "Mindste skrifttype som stadig er ordentligt læsbar."

#~ msgid "Used to display text beside toolbar icons."
#~ msgstr "Brugt til at vise tekst ved siden af værktøjslinje-ikoner."

#~ msgid "Used by menu bars and popup menus."
#~ msgstr "Brugt af menulinjer og popop-menuer."

#~ msgid "Used by the window titlebar."
#~ msgstr "Brugt af vinduets titellinje."

#~ msgctxt "Font role"
#~ msgid "%1: "
#~ msgstr "%1: "

#~ msgid "Click to change all fonts"
#~ msgstr "Klik for at ændre alle skrifttyper"

#~ msgctxt "Use anti-aliasing"
#~ msgid "System Settings"
#~ msgstr "Systemindstillinger"

#~ msgid "Select this option to smooth the edges of curves in fonts."
#~ msgstr ""
#~ "Vælg denne indstilling for at udglatte kanterne på kurver i skrifttyper."

#~ msgctxt "use system subpixel setting"
#~ msgid "System default"
#~ msgstr "Systemets standard"

#~ msgctxt "use system hinting settings"
#~ msgid "System default"
#~ msgstr "Systemets standard"

#~ msgctxt "font usage"
#~ msgid "Taskbar"
#~ msgstr "Opgavelinje"

#~ msgctxt "font usage"
#~ msgid "Desktop"
#~ msgstr "Skrivebord"

#~ msgid "Used by the taskbar."
#~ msgstr "Brugt af opgavelinjen."

#~ msgid "Used for desktop icons."
#~ msgstr "Bruges til skrivebordsikoner."

#~ msgctxt "Force fonts DPI"
#~ msgid "Disabled"
#~ msgstr "Deaktiveret"

#~ msgid "96 DPI"
#~ msgstr "96 punkter per tomme"

#~ msgid "120 DPI"
#~ msgstr "120 punkter per tomme"
