# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2017, 2019, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2022-09-24 15:51+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: package/contents/ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "Tryck för att välja plats på kartan."

#: package/contents/ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "Tryck för att välja plats på kartan."

#: package/contents/ui/LocationsFixedView.qml:78
#: package/contents/ui/LocationsFixedView.qml:103
#, kde-format
msgid "Zoom in"
msgstr "Zooma in"

#: package/contents/ui/LocationsFixedView.qml:222
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"Modifierad från <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>Världplatskarta</link> av "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"

#: package/contents/ui/LocationsFixedView.qml:235
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Latitud:"

#: package/contents/ui/LocationsFixedView.qml:261
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Longitud:"

#: package/contents/ui/main.qml:87
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr "Det blåa ljusfiltret gör skärmens färger varmare."

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Switching times:"
msgstr "Bytestider:"

#: package/contents/ui/main.qml:100
#, kde-format
msgid "Always off"
msgstr "Alltid av"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "Solnedgång och soluppgång på nuvarande plats"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "Solnedgång och soluppgång på manuell plats"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Custom times"
msgstr "Egna tider"

#: package/contents/ui/main.qml:104
#, kde-format
msgid "Always on night color"
msgstr "Nattfärg alltid på"

#: package/contents/ui/main.qml:156
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"Apparatens plats uppdateras periodiskt genom att använda GPS (om "
"tillgängligt), eller genom att skicka nätverksinformation till <link "
"url='https://location.services.mozilla.com'>Mozilla Location Service</link>."

#: package/contents/ui/main.qml:175
#, kde-format
msgid "Day color temperature:"
msgstr "Dagfärgtemperatur:"

#: package/contents/ui/main.qml:218 package/contents/ui/main.qml:277
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1 K"

#: package/contents/ui/main.qml:222 package/contents/ui/main.qml:281
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "Varm"

#: package/contents/ui/main.qml:228 package/contents/ui/main.qml:287
#, kde-format
msgctxt "No blue light filter activated"
msgid "Cool (no filter)"
msgstr "Kall (inget filter)"

#: package/contents/ui/main.qml:228 package/contents/ui/main.qml:287
#, kde-format
msgctxt "Night colour blue-ish"
msgid "Cool"
msgstr "Kall"

#: package/contents/ui/main.qml:234
#, kde-format
msgid "Night color temperature:"
msgstr "Nattfärgtemperatur:"

#: package/contents/ui/main.qml:298
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "Latitud: %1°  Longitud: %2°"

#: package/contents/ui/main.qml:308
#, kde-format
msgid "Begin night color at:"
msgstr "Starta nattfärg:"

#: package/contents/ui/main.qml:321 package/contents/ui/main.qml:344
#, kde-format
msgid "Input format: HH:MM"
msgstr "Indataformat: TT:MM"

#: package/contents/ui/main.qml:331
#, kde-format
msgid "Begin day color at:"
msgstr "Starta dagfärg:"

#: package/contents/ui/main.qml:353
#, kde-format
msgid "Transition duration:"
msgstr "Övergångstid:"

#: package/contents/ui/main.qml:362
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minut"
msgstr[1] "%1 minuter"

#: package/contents/ui/main.qml:375
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "Inmatade minuter - min 1, max 600"

#: package/contents/ui/main.qml:393
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Fel: Övergångstider överlappar."

#: package/contents/ui/main.qml:413
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "Lokaliserar…"

#: package/contents/ui/TimingsView.qml:32
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr "Färgtemperaturen börjar ändras till nattid %1 och är helt ändrad %2"

#: package/contents/ui/TimingsView.qml:39
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr "Färgtemperaturen börjar ändras till dagtid %1 och är helt ändrad %2"

#, fuzzy
#~| msgid "This is what Night Color will look like when active."
#~ msgid "This is what day color temperature will look like when active."
#~ msgstr "Så här ser Nattfärg ut när den är aktiv."

#, fuzzy
#~| msgid "This is what Night Color will look like when active."
#~ msgid "This is what night color temperature will look like when active."
#~ msgstr "Så här ser Nattfärg ut när den är aktiv."

#, fuzzy
#~| msgid "Activate Night Color"
#~ msgid "Activate blue light filter"
#~ msgstr "Aktivera nattfärg"

#~ msgid "Turn on at:"
#~ msgstr "Sätt på:"

#~ msgid "Turn off at:"
#~ msgstr "Stäng av:"

#~ msgid "Night Color begins changing back at %1 and ends at %2"
#~ msgstr "Nattfärg börjar ändras tillbaka %1 och slutar %2"

#~ msgid "Error: Morning is before evening."
#~ msgstr "Fel: Morgonen kommer före kvällen."

#~ msgid "Detect Location"
#~ msgstr "Detektera plats"

#~ msgid ""
#~ "The device's location will be detected using GPS (if available), or by "
#~ "sending network information to <a href=\"https://location.services."
#~ "mozilla.com\">Mozilla Location Service</a>."
#~ msgstr ""
#~ "Apparatens plats detekteras genom att använda GPS (om tillgängligt), "
#~ "eller genom att skicka nätverksinformation till <a href=\"https://"
#~ "location.services.mozilla.com\">Mozilla Location Service</a>."

#~ msgid "Night Color begins at %1"
#~ msgstr "Nattfärg börjar %1"

#~ msgid "Color fully changed at %1"
#~ msgstr "Färg helt ändrad %1"

#~ msgid "Normal coloration restored by %1"
#~ msgstr "Normal färgläggning återställd %1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Stefan Asserhäll"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "stefan.asserhall@bredband.net"

#~ msgid "Night Color"
#~ msgstr "Nattfärg"

#~ msgid "Roman Gilg"
#~ msgstr "Roman Gilg"

#~ msgid " K"
#~ msgstr " K"

#~ msgid "Operation mode:"
#~ msgstr "Användningsmetod:"

#~ msgid "Automatic"
#~ msgstr "Automatisk"

#~ msgid "Times"
#~ msgstr "Tider"

#~ msgid "Constant"
#~ msgstr "Konstant"

#~ msgid "Sunrise begins:"
#~ msgstr "Soluppgång börjar:"

#~ msgid "(Input format: HH:MM)"
#~ msgstr "(indataformat: TT:MM)"

#~ msgid "Sunset begins:"
#~ msgstr "Solnedgång börjar:"

#~ msgid "...and ends:"
#~ msgstr "... och slutar:"

#~ msgid "Manual"
#~ msgstr "Manuell"

#~ msgid "(HH:MM)"
#~ msgstr "(TT:MM)"
